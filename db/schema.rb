# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131020001402) do

  create_table "titles_movies_series", :force => true do |t|
    t.text    "title",                         :null => false
    t.string  "imdb_index",      :limit => 12
    t.integer "kind_id",                       :null => false
    t.integer "production_year"
    t.integer "imdb_id"
    t.string  "phonetic_code",   :limit => 5
    t.integer "episode_of_id"
    t.integer "season_nr"
    t.integer "episode_nr"
    t.string  "series_years",    :limit => 49
    t.string  "md5sum",          :limit => 32
  end

  create_table "users", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "fname"
    t.string   "lname"
    t.string   "email"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "v_quotes", :force => true do |t|
    t.string   "youtubeId"
    t.string   "vTitle"
    t.string   "vDescription"
    t.integer  "vStart"
    t.integer  "vEnd"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "movieId"
    t.string   "lang"
    t.string   "actorId"
    t.string   "charId"
    t.string   "movieName"
    t.string   "charName"
    t.string   "actorName"
    t.string   "userId"
  end

  create_table "vquotes", :force => true do |t|
    t.string   "youtubeId"
    t.string   "vTitle"
    t.string   "vDescription"
    t.integer  "vStart"
    t.integer  "vEnd"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

end
