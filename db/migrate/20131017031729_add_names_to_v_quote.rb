class AddNamesToVQuote < ActiveRecord::Migration
  def change
    add_column :v_quotes, :movieName, :string
    add_column :v_quotes, :charName, :string
    add_column :v_quotes, :actorName, :string
  end
end
