class AddProperitesToVQuote < ActiveRecord::Migration
  def change
    add_column :v_quotes, :movieId, :string
    add_column :v_quotes, :lang, :string
    add_column :v_quotes, :actorId, :string
    add_column :v_quotes, :charId, :string
  end
end
