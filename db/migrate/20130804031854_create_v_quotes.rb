class CreateVQuotes < ActiveRecord::Migration
  def change
    create_table :v_quotes do |t|
      t.string :youtubeId
      t.string :vTitle
      t.string :vDescription
      t.integer :vStart
      t.integer :vEnd

      t.timestamps
    end
  end
end
