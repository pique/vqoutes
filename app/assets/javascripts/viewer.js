(function ($) {
  $(document).ready( function() {

    function selectAll(id) {
        var textBox = document.getElementById(id);
        textBox.readOnly = true;
        textBox.onmouseup = function() {
            textBox.select();
            return false;
        };
        textBox.onmousedown = function() {
            textBox.select();
            return false;
        };
    };

    selectAll('copyLink');
    var defaultOptions = {
    };

    var methods = {
      init : function(options) {
        $.getScript("https://www.youtube.com/iframe_api");
      },
      loadVideo : function(data) {
        if (data) {
        console.log(data);
        window.viewerPlayer.loadVideoById({'videoId': data.youtubeId, 'startSeconds': data.start, 'endSeconds': data.end, 'suggestedQuality': data.suggestedQuality});
        $("#copyLink").val("http://www.vquoto.com/quoto/" + data.id);
        $("#quoteTitle").text(data.title);
        $("#quoteMovie").text(data.mName);
        $("#quoteActor").text(data.aName);
        $("#infoContainer").show();
        }
      }
    }

    $.fn.viewer = function(method) {
      if (methods[method]) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
      } else if ( typeof method === 'object' || !method) {
        return methods.init.apply(this, arguments);
      } else {
        $.error('Method ' + method + ' does not exist on jQuery.viewer');
      }
    };
  });
})(jQuery);
(function ($) {
  $(document).ready( function() {
    $("#viewer").viewer();
  });
})(jQuery);
