(function ($) {
  $(document).ready( function() {

    $('#searchBox').keyup(function(e) {
      clearTimeout($.data(this, 'timer'));
      if (e.keyCode == 13) {
            search(true);
      } else {
        $(this).data('timer', setTimeout(search, 500));
      }
    });
    function search(force) {
        var existingString = $("#searchBox").val();
        $.ajax({
        url: "/home/search",
        dataType: "html",
        data: {key : existingString },
        success: function (msg) {
          $("#gallery").empty();
          $("#gallery").html(msg);
        }
        });
    }

    var timer;

    $("body").on( {
      mouseenter : function () {

        var img = $(this).prev().children()[0];
        $source = $(this);
        $source.children().show();
       // $source.css('background', 'linear-gradient(to bottom, rgba(0,0,0,1) 0%,rgba(0,0,0,1) 13%,rgba(0,0,0,0.24) 100%)');

        timer = setTimeout(function () {
          $(img).animate({
             left: "+=50",
          }, 500, function() {
          });
        }, 200);


      },
    mouseleave : function() {
        var img = $(this).prev().children()[0];
        clearTimeout(timer);

        $(img).animate({
           left: "-100px",
        }, 100, function() {
        });
      //  $(this).css('background', 'none');
      //  $(this).children().hide();

      }
    }, ".vInfoContainer");

    $("body").on( {
      click : function () {
        console.log("Loading");

      var data = {

        'id' : $(this).attr("data-id"),
        'youtubeId' : $(this).attr("data-youtubeId"),
        'start' : $(this).attr("data-start"),
        'end' : $(this).attr("data-end"),
        'title' : $(this).attr("data-title"),
        'mName' : $(this).attr("data-mName"),
        'aName' : $(this).attr("data-aName"),
        'suggestedQuality': 'large'
      };
      $("#viewer").viewer("loadVideo", data);
      }
    }, ".vInfoContainer");

  });
})(jQuery);
