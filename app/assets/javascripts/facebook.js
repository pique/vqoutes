(function ($) {
  $(document).ready( function() {
    window.fbAsyncInit = function() {
      // init the FB JS SDK
      /*FB.init({
        appId : '1377638272475800', // App ID from the App Dashboard
        status : true, // check the login status upon init?
        cookie : true, // set sessions cookies to allow your server to access the session?
        xfbml : true // parse XFBML tags on this page?
      });*/
    };
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1377638272475800";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    
    $('#signInButton').click(function (e) {
      e.preventDefault();
      FB.getLoginStatus(function(response) {
        if(response.status == "connected") {
          location.href = '/auth/facebook';
        } else {
          FB.login(function(response) {
            if (response.authResponse) {
              window.location = "/auth/facebook";
            } else {
            }
          });
        }
      });
    });
    $('#signOutButton').click(function (e) {
      FB.getLoginStatus(function(response) {
          FB.logout(response);
      });
    });
  });
})(jQuery);
