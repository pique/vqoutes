var quoteCreator = function ($) {
    var platButtonClicked = false;
    var selectedMovieId ='';
    var selectedMovieName ='';
    var extractedYoutubeId;
    var startTimeInseconds;
    var endTimeInseconds;
	 
    function convertToseconds(time) {
      var timeSplit = time.split(':');
      var minutes = parseInt(timeSplit[0]);
      var seconds = parseInt(timeSplit[1]);
      return (minutes * 60) + seconds;
    };

    function extractYoutubeId(link) {
      return link.replace('http://www.youtube.com/watch?v=', '');
    };

    function getMovie() {
        var currentMovieName = $("#movieName").val();
        if (selectedMovieId === '' || selectedMovieName !== currentMovieName) {
            return {name: currentMovieName}
        } else {
            return {name:currentMovieName, id:selectedMovieId}
        }
    };

    $('#youtubeUrl').on('input', function() {
       platButtonClicked = false;
    });
    $('#startTime').on('input', function() {
       platButtonClicked = false;
    });
    $('#endTime').on('input', function() {
       platButtonClicked = false;
    });
    $("#loadButton").click(function() {
      var youtubeId = $("#youtubeUrl").val();
      var startTime = $("#startTime").val();
      var endTime = $("#endTime").val();
      if (youtubeId == '') {
        $("#notification").text("Youtube url cannot be empty. Please?");
        $("#notification").css('visibility', 'visible');
        platButtonClicked = false;
        return;
      } 

      if (startTime == '') {
        startTime = "0:00"
      }

      if (endTime == '') {
        endTime = "0:00"
      }

      extractedYoutubeId = extractYoutubeId(youtubeId);
      startTimeInseconds = convertToseconds(startTime);
      endTimeInseconds = convertToseconds(endTime);
      console.log(extractedYoutubeId + "--" + startTimeInseconds + "--" + endTimeInseconds);

      if ((endTimeInseconds - startTimeInseconds) === 0 ) {
        $("#notification").text("Sorry! The length is zero.");
        $("#notification").css('visibility', 'visible');
        platButtonClicked = false;
        return;
      }
      platButtonClicked = true;

      $("#notification").css('visibility', 'hidden');

      $("#createVideoPlayer").attr("src", "https://www.youtube.com/embed/" + extractedYoutubeId + "?start=" + startTimeInseconds + "&end=" + endTimeInseconds + 
        "&autoplay=1&controls=1&showinfo=0&iv_load_policy=3&rel=0&modestBranding=1&version=3&enablejsapi=1&origin=darkonyx.com&wmode=opaque");
    });
    
/*    $("#movieName").autocomplete({
       focus: function( event, ui ) {
         $( "#movieName" ).val( ui.item.title);
         return false;
       },
     	select: function( event, ui ) {
                $("#movieName").val(ui.item.title);
     		selectedMovieId = ui.item.id;
     		selectedMovieName = ui.item.title;
                console.log(selectedMovieId);
     		return false;
     	},
     	
     	source: function( request, response ) {
                var selectedMovieId ='';
                var selectedMovieName ='';
     		$.ajax({
     			url: "/v_quote/autocomplete",
                data: {searchKey : request.term },
                dataType: "json",
                success: function (msg) {
                  var movieNames = [];
                  $.each(msg, function(i, v) {
                    movieNames.push({title : v.title, id : v.id});
                  });
                  console.log(movieNames);
                  response(movieNames);
                }
            });
         }
     })
     .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
     	return $( "<li>" ).append( "<a>" + item.title + "</a>").appendTo(ul);
    };;
  */  
    $("#uploadButton").click(function() {
      var youtubeId = $("#youtubeUrl").val();
      var vTitle = $("#title").val();
      var vStart = $("#startTime").val();
      var vEnd = $("#endTime").val();
      var actorName = $("#actorName").val();
      var movie = getMovie();
      var movieName = movie.name;
      var movieId = movie.id;

      if (typeof extractedYoutubeId === "undefined" ||
          typeof startTimeInseconds === "undefined" ||
          typeof endTimeInseconds === "undefined" ) {
            $("#notification").text("You did not click the play button? How will you know the quote is sliced properly ?");
            $("#notification").css('visibility', 'visible');
            return;

          }
      if (vTitle === '') {
            $("#notification").text("Set the Description. Please?");
            $("#title").css("border-bottom", "1px solid red");
            $("#notification").css('visibility', 'visible');
            return;
      }
      $("#title").css("border-bottom", "1px solid #E5E5E5");
      if (movieName === '') {
            $("#notification").text("Set the Movie Name. Please?");
            $("#movieName").css("border-bottom", "1px solid red");
            $("#notification").css('visibility', 'visible');
            return;
      }
      $("#movieName").css("border-bottom", "1px solid #E5E5E5");

      $("#notification").css('visibility', 'hidden');


      console.log("UPLOAD" + youtubeId + "--" + startTime + "--" + endTime);
      $.ajax({
        url: "/v_quote/create",
        data: {
          vTitle: vTitle,
          youtubeId: extractedYoutubeId,
          vStart: startTimeInseconds,
          vEnd: endTimeInseconds,
          aName: actorName,
          mName : movieName,
          mId: movieId
        }
      })
      .done(function(data) {
          window.location = "/?id=" + data;
      });
    });
};
  $(document).ready( function() {
    $.getScript("https://www.youtube.com/iframe_api");
    console.log("Creating quoter");
    quoteCreator(jQuery);
  });
    
    
