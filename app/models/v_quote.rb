class VQuote < ActiveRecord::Base
  attr_accessible :vDescription, :vEnd, :vStart, :vTitle, :youtubeId

  ORIGIN = "http://www.vquoto.com"
  YOUTUBE_VIDEO_URL = "https://www.youtube.com/watch?v=";
  YOUTUBE_IMAGE_URL = "https://img.youtube.com/vi/";
  YOUTUBE_IMAGE_ID = "/0.jpg";

  def youtube_url
    return YOUTUBE_VIDEO_URL + youtubeId;
  end

  def image_url
    return YOUTUBE_IMAGE_URL + youtubeId + YOUTUBE_IMAGE_ID;
  end

  def sliced_url

    # Create a configurable youtube class.
    return "https://www.youtube.com/v/" + 
      youtubeId + get_youtube_configs
  end
  
  def embed_url

    # Create a configurable youtube class.
    return "https://www.youtube.com/embed/" + 
      youtubeId + get_youtube_configs
  end


  private 

  def get_youtube_configs
    return "?start=" + vStart.to_s + "&end=" + vEnd.to_s + 
      "&autoplay=1&controls=0&showinfo=0&iv_load_policy=3&rel=0&modestBranding=1&version=3&enablejsapi=1&origin=" + ORIGIN + "&wmode=opaque";
  end
end
