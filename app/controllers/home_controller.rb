class HomeController < ApplicationController
  def show
    @v_quotes = VQuote.all
    @loadVideo = nil

    if (params[:id] != nil) 
      @loadVideo = VQuote.find_by_id(params[:id])
    end

  end

  def viewer
  end

  def search
    puts params
    @v_quotes = VQuote.where("movieName LIKE ?", params[:key] + "%")
    respond_to do |format|
      format.json {
      render :json => render_to_string(:layout => false, :action => "_gallery", :formats => :html, :collection =>  @v_quotes)
    }
 end

  end

  def mine
    if current_user
      @v_quotes = VQuote.where(:userId => current_user.id)
      puts current_user
      puts @v_quotes
      render 'show'
    else 
      redirect_to "/"
    end
  end
end
