class VQuoteController < ApplicationController
  def show
    @video = VQuote.find_by_id(params[:id])
  end

  def new
    if !current_user
      redirect_to "/"
    end
  end

  def create
    #TODO : Validate params

    if current_user
      if params[:youtubeId] == nil
        redirect_to "/"
      else
      vq = VQuote.new

      puts params
      vq.vTitle = params[:vTitle]
      vq.youtubeId = params[:youtubeId]
      vq.vStart = params[:vStart]
      vq.vEnd = params[:vEnd]
      vq.movieName = params[:mName]
      vq.actorName = params[:aName]
      vq.movieId = params[:mId]
      vq.userId = current_user.id
      vq.save
     
      respond_to do |format|
        format.json {
          render :json => vq.id
      }
    end
    end
    end

  end

  def autocomplete
    @titles = MovieTitles.where("title LIKE ?", params[:searchKey] + "%").limit(10)
    respond_to do |format|
      format.json {
        render :json => @titles.to_json
      }
    end
  end

  def delete
  end
end
